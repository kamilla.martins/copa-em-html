let jogos = [];
class Jogo{
  constructor(diaJogo, estadio, mandante, visitante, arbitro){
    this.diaJogo = diaJogo;
    this.estadio = estadio;
    this.mandante = mandante;
    this.visitante = visitante;
    this.arbitro = arbitro;
  }
}
function cadastrarJogos(){
  let diaJogo = byId("dia").value;
  let estadio = byId("estadio").value;
  let mandante = byId("mandante").value;
  let visitante = byId("visitante").value;
  let arbitro = byId("arbitro").value;
  let jogo = new Jogo(diaJogo, estadio, mandante, visitante, arbitro);
  jogos.push(jogo);
  apresentarJogos();
}
function apresentarJogos(){
  let tabela = byId("tabela");
  let listagem  = "<ol>"
  for(let jogo of jogos) {
    listagem += `<li>${jogo.diaJogo} ;${jogo.estadio}; ${jogo.mandante}; ${jogo.visitante} ;${jogo.arbitro}</li>`
  }
  listagem += "</ol>" 
  tabela.innerHTML = listagem;
}

function byId(id) {
  return document.getElementById(id);
}